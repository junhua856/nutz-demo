package net.wendal.nutzbook.web;

import javax.websocket.DeploymentException;
import javax.websocket.server.ServerContainer;

import net.wendal.nutzbook.core.CoreMainSetup;
import org.nutz.ioc.Ioc;
import org.nutz.mvc.NutConfig;
import org.nutz.mvc.Setup;
import org.nutz.plugins.hotplug.Hotplug;

import net.wendal.nutzbook.core.websocket.NutzbookWebsocket;

import java.util.ArrayList;
import java.util.List;

public class MainSetup implements Setup {

    @Override
    public void init(NutConfig nc) {
        Ioc ioc = nc.getIoc();
        ioc.get(Hotplug.class).setupInit();
    }

    @Override
    public void destroy(NutConfig nc) {
        Ioc ioc = nc.getIoc();
        ioc.get(Hotplug.class).setupDestroy();
    }

//    protected CoreMainSetup core;
//    protected List<Setup> setups;
//
//    @Override
//    public void init(NutConfig nc) {
//        core = new CoreMainSetup();
//        core.init(nc);
//        setups = new ArrayList<>();
//        for (String name : nc.getIoc().getNamesByType(Setup.class)) {
//            Setup setup = nc.getIoc().get(Setup.class, name);
//            setups.add(setup);
//            setup.init(nc);
//        }
//    }
//
//    @Override
//    public void destroy(NutConfig nc) {
//        core.destroy(nc);
//        for (Setup setup : setups) {
//            setup.destroy(nc);
//        }
//    }

}
