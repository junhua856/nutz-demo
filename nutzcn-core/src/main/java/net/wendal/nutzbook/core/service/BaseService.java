package net.wendal.nutzbook.core.service;

import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.service.EntityService;

/**
 * Created by hj on 2019/3/08.
 */
@IocBean(args={ "refer:dao"})
public class BaseService<T> extends EntityService<T>{

}
